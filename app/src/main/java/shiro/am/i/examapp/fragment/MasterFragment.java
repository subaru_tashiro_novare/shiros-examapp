package shiro.am.i.examapp.fragment;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import shiro.am.i.examapp.R;
import shiro.am.i.examapp.activity.MainActivity;
import shiro.am.i.examapp.adapter.MasterAdapter;
import shiro.am.i.examapp.functionalinterface.Consumer;
import shiro.am.i.examapp.viewmodel.MainViewModel;

public class MasterFragment extends Fragment {

    private Consumer<Integer> onItemClickListener;

    private MainViewModel viewModel;

    public static Fragment newInstance() {
        return new MasterFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MainActivity mainActivity = (MainActivity) context;
        onItemClickListener = mainActivity::onItemClick;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentActivity activity = requireActivity();
        viewModel = ViewModelProviders.of(activity).get(MainViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_master, container, false);

        MasterAdapter adapter = new MasterAdapter(inflater, onItemClickListener);

        RecyclerView cityRecycler = view.findViewById(R.id.city_recycler);
        cityRecycler.setHasFixedSize(true);
        cityRecycler.setAdapter(adapter);

        viewModel.getData().observe(this, adapter::submitList);

        return view;
    }
}
