package shiro.am.i.examapp.retrofit;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import retrofit2.http.GET;
import shiro.am.i.examapp.BuildConfig;
import shiro.am.i.examapp.model.JsonResponse;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;

public class OpenWeatherMap {

    public static final Api API = buildApi();

    private static Api buildApi() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor()
                .setLevel(BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();

        return new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create(Api.class);
    }

    public interface Api {

        @GET("group?id=2643743,3067696,5391959&units=metric&appid=ec6935930c66d8562ec2451a73865664")
        Single<JsonResponse> getHome();
    }
}
