package shiro.am.i.examapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import shiro.am.i.examapp.R;
import shiro.am.i.examapp.functionalinterface.Consumer;
import shiro.am.i.examapp.model.BusinessItem;

public class MasterAdapter extends ListAdapter<BusinessItem, MasterAdapter.ViewHolder> {

    private final LayoutInflater inflater;
    private final Consumer<Integer> onClickListener;

    public MasterAdapter(LayoutInflater inflater, Consumer<Integer> onClickListener) {
        super(new DiffCallback());
        this.inflater = inflater;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_city, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BusinessItem item = getItem(position);
        holder.bind(item);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView locationText;
        private final TextView weatherText;
        private final TextView temperatureText;

        private ViewHolder(View itemView) {
            super(itemView);
            locationText = itemView.findViewById(R.id.location);
            weatherText = itemView.findViewById(R.id.weather);
            temperatureText = itemView.findViewById(R.id.temperature);
            itemView.setOnClickListener(v -> onItemClick());
        }

        private void bind(BusinessItem item) {
            locationText.setText(item.getLocation());
            weatherText.setText(item.getWeather());
            temperatureText.setText(item.getTemperature());
        }

        private void onItemClick() {
            onClickListener.accept(getAdapterPosition());
        }
    }

    private static class DiffCallback extends DiffUtil.ItemCallback<BusinessItem> {

        @Override
        public boolean areItemsTheSame(BusinessItem oldItem, BusinessItem newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(BusinessItem oldItem, BusinessItem newItem) {
            // TODO: 5/22/2018
            return false;
        }
    }
}
