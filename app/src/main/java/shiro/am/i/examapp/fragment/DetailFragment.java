package shiro.am.i.examapp.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import shiro.am.i.examapp.R;
import shiro.am.i.examapp.model.BusinessItem;
import shiro.am.i.examapp.viewmodel.MainViewModel;

public class DetailFragment extends Fragment {

    private static String KEY = "position";

    private MainViewModel viewModel;

    public static Fragment newInstance(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEY, position);

        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(bundle);
        return detailFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentActivity activity = requireActivity();
        viewModel = ViewModelProviders.of(activity).get(MainViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int position = getArguments().getInt(KEY);

        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        viewModel.getData().observe(this, businessItems -> {
            BusinessItem item = viewModel.getItem(position);
            bindData(view, item);
        });

        return view;
    }

    private void bindData(View view, BusinessItem item) {
        ImageView weatherImage = view.findViewById(R.id.weather_image);
        Glide.with(this)
                .load(item.getIconUrl())
                .into(weatherImage);

        TextView locationText = view.findViewById(R.id.location);
        locationText.setText(item.getLocation());

        TextView weatherText = view.findViewById(R.id.weather);
        weatherText.setText(item.getWeather());

        TextView temperatureText = view.findViewById(R.id.temperature);
        temperatureText.setText(item.getTemperature());

        TextView windText = view.findViewById(R.id.wind_value);
        windText.setText(item.getWind());

        TextView pressureText = view.findViewById(R.id.pressure_value);
        pressureText.setText(item.getPressure());

        TextView humidityText = view.findViewById(R.id.humidity_value);
        humidityText.setText(item.getHumidity());

        TextView coordsText = view.findViewById(R.id.coords_value);
        coordsText.setText(item.getCoords());
    }
}
