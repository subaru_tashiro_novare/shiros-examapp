package shiro.am.i.examapp.model;

import com.squareup.moshi.Json;

import java.util.List;

/**
 * Intentionally without getters/setters
 */
public class JsonResponse {

    @Json(name = "cnt") public int cnt;
    @Json(name = "list") public List<City> list;

    public static class City {

        @Json(name = "id") public int id;
        @Json(name = "coord") public Coord coord;
        @Json(name = "sys") public Sys sys;
        @Json(name = "weather") public List<Weather> weather;
        @Json(name = "main") public Main main;
        @Json(name = "visibility") public int visibility;
        @Json(name = "wind") public Wind wind;
        @Json(name = "clouds") public Clouds clouds;
        @Json(name = "dt") public int dt;
        @Json(name = "name") public String name;
    }

    public static class Coord {

        @Json(name = "lon") public double lon;
        @Json(name = "lat") public double lat;
    }

    public static class Sys {

        @Json(name = "type") public int type;
        @Json(name = "id") public int id;
        @Json(name = "message") public double message;
        @Json(name = "country") public String country;
        @Json(name = "sunrise") public int sunrise;
        @Json(name = "sunset") public int sunset;
    }

    public static class Weather {

        @Json(name = "id") public int id;
        @Json(name = "main") public String main;
        @Json(name = "description") public String description;
        @Json(name = "icon") public String icon;
    }

    public static class Main {

        @Json(name = "temp") public double temp;
        @Json(name = "pressure") public int pressure;
        @Json(name = "humidity") public int humidity;
        @Json(name = "temp_min") public double tempMin;
        @Json(name = "temp_max") public double tempMax;
    }

    public static class Wind {

        @Json(name = "speed") public double speed;
        @Json(name = "deg") public int deg;
    }

    public static class Clouds {

        @Json(name = "all") public int all;
    }
}
