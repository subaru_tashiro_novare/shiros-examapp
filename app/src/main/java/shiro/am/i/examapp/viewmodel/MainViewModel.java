package shiro.am.i.examapp.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import io.reactivex.disposables.Disposable;
import io.realm.Realm;
import io.realm.RealmResults;
import shiro.am.i.examapp.model.BusinessItem;
import shiro.am.i.examapp.retrofit.OpenWeatherMap;
import timber.log.Timber;

import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;

public class MainViewModel extends ViewModel {

    private final Realm realm;

    private final MutableLiveData<List<BusinessItem>> data = new MutableLiveData<>();

    private Disposable disposable;

    public MainViewModel() {
        realm = Realm.getDefaultInstance();

        RealmResults<BusinessItem> managedCities = realm.where(BusinessItem.class).findAll();
        List<BusinessItem> unmanagedCities = realm.copyFromRealm(managedCities);
        data.setValue(unmanagedCities);

        queryData();
    }

    @Override
    protected void onCleared() {
        disposable.dispose();
        realm.close();
    }

    public LiveData<List<BusinessItem>> getData() {
        return data;
    }

    public void queryData() {
        disposable = OpenWeatherMap.API.getHome()
                .flattenAsObservable(response -> response.list)
                .map(BusinessItem::new)
                .toList()
                .observeOn(mainThread())
                .subscribe(this::onSuccess, Timber::e);
    }

    public BusinessItem getItem(int position) {
        return data.getValue().get(position);
    }

    private void onSuccess(List<BusinessItem> response) {
        data.setValue(response);
    }
}
