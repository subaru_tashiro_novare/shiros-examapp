package shiro.am.i.examapp.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import shiro.am.i.examapp.R;

public class SplashActivity extends AppCompatActivity {

    private static final long DELAY = 2000;

    private Handler delayHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onStart() {
        super.onStart();
        delayHandler.postDelayed(this::invokeMainActivity, DELAY);
    }

    @Override
    protected void onStop() {
        super.onStop();
        delayHandler.removeCallbacksAndMessages(null);
    }

    private void invokeMainActivity() {
        MainActivity.start(this);
        finish();
    }
}
