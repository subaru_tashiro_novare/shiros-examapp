package shiro.am.i.examapp.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import shiro.am.i.examapp.R;
import shiro.am.i.examapp.fragment.DetailFragment;
import shiro.am.i.examapp.fragment.MasterFragment;
import shiro.am.i.examapp.viewmodel.MainViewModel;

public class MainActivity extends AppCompatActivity {

    private MainViewModel viewModel;

    public static void start(Context context) {
        Intent starter = new Intent(context, MainActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        setContentView(R.layout.activity_master);

        Fragment fragment = MasterFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_frame, fragment)
                .commit();
    }

    public void onItemClick(int position) {
        Fragment fragment = DetailFragment.newInstance(position);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_frame, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.refresh) {
            viewModel.queryData();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
