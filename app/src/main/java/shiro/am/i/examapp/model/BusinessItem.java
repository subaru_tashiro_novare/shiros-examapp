package shiro.am.i.examapp.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class BusinessItem extends RealmObject {

    @PrimaryKey
    private int id;

    private String location;

    private String weather;

    private String temperature;

    private String iconUrl;

    private String wind;

    private String pressure;

    private String humidity;

    private String coords;

    public BusinessItem() {
        // default constructor required by Realm
    }

    public BusinessItem(JsonResponse.City city) {
        location = String.format("%s, %s", city.name, city.sys.country);
        weather = city.weather.get(0).description;
        temperature = Double.toString(city.main.temp) + "°С";
        iconUrl = String.format("http://openweathermap.org/img/w/%s.png", city.weather.get(0).icon);
        wind = String.format("%s m/s", city.wind.speed);
        pressure = String.format("%s hpa", city.main.pressure);
        humidity = String.format("%s %%", city.main.humidity);
        coords = String.format("[%s,%s]", city.coord.lat, city.coord.lon);
    }

    public String getLocation() {
        return location;
    }

    public String getWeather() {
        return weather;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getWind() {
        return wind;
    }

    public String getPressure() {
        return pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getCoords() {
        return coords;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BusinessItem that = (BusinessItem) o;

        return id == that.id;
    }
}
